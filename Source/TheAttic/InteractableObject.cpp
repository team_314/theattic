// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableObject.h"
#include "Kismet/GameplayStatics.h"

AInteractableObject::AInteractableObject()
{
	BoxComponent = CreateDefaultSubobject<UBoxComponent>("Collision Box");
	BoxComponent->SetupAttachment(RootComponent);
}

void AInteractableObject::Interact()
{
	UGameplayStatics::SpawnEmitterAtLocation(this, ParticleHit, GetActorLocation());
	Destroy();
}
