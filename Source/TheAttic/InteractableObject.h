// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperSpriteActor.h"
#include "Components/BoxComponent.h"
#include "TheAttic/Interactable.h"
#include "InteractableObject.generated.h"

/**
 * 
 */
UCLASS()
class THEATTIC_API AInteractableObject : public APaperSpriteActor, public IInteractable
{
	GENERATED_BODY()
	
public:
	AInteractableObject();

	virtual void Interact() override;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UBoxComponent* BoxComponent = nullptr;

	UPROPERTY(EditAnywhere, Category = Effects)
	UParticleSystem* ParticleHit = nullptr;
};
