// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TheAttic : ModuleRules
{
	public TheAttic(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });
	}
}
