// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TheAtticGameMode.generated.h"

UCLASS(minimalapi)
class ATheAtticGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATheAtticGameMode();
};



